# Nginx
#
# VERSION 0.0.1
FROM python:3.6-alpine
 
EXPOSE 8000

# djangoユーザを作成して利用

RUN adduser -D django
USER django
RUN mkdir -p /home/django/.local/bin

# PATHにユーザ固有のpip install先を含める

ENV PATH=$PATH:/home/django/.local/bin
COPY --chown=django:django requirements.txt ./

# requirementsをユーザ領域にインストール
RUN pip install --user -r requirements.txt

# アプリケーション本体をコンテナ内にデプロイ
#COPY --chown=django:django ./sampleapp /home/django/sampleapp
#WORKDIR /home/django/sampleapp
# コンテナ外部からもアプリケーションにアクセスできるように0.0.0.0:8000を指定
#CMD ["python", "manage.py" , "runserver", "0.0.0.0:8000"]
